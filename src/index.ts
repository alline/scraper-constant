import { EpisodeContext, EpisodeResult, EpisodeScraper } from "@alline/core";
import { Episode } from "@alline/model";
import _ from "lodash";

export interface ConstantEpisodeScraperOption {
  constant?: Partial<Episode>;
  episodeConstant?: {
    [key: number]: Partial<Episode>;
  };
}

export class ConstantEpisodeScraper extends EpisodeScraper {
  private constant: Partial<Episode>;
  private episodeConstant: {
    [key: number]: Partial<Episode>;
  };

  constructor(option: ConstantEpisodeScraperOption) {
    super({});
    const { constant = {}, episodeConstant = {} } = option;
    this.constant = constant;
    this.episodeConstant = episodeConstant;
  }

  protected async onScrap(
    context: EpisodeContext,
    result: EpisodeResult
  ): Promise<EpisodeResult> {
    const { episode, logger } = context;
    const constant = this.constant;
    const episodeConstant = this.episodeConstant[episode];
    logger.debug("onScrap", {
      label: "ConstantEpisodeScraper",
      constant,
      episodeConstant
    });
    const { data, thumbnails } = result;
    return {
      data: _.merge({}, data, constant, episodeConstant),
      thumbnails
    };
  }
}
