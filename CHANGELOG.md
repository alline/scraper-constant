## [1.1.3](https://gitlab.com/alline/scraper-constant/compare/v1.1.2...v1.1.3) (2020-06-29)


### Bug Fixes

* update dependencies ([1e1be65](https://gitlab.com/alline/scraper-constant/commit/1e1be65aa5d325a7537f4b522e659c20d98ccb6d))

## [1.1.2](https://gitlab.com/alline/scraper-constant/compare/v1.1.1...v1.1.2) (2020-06-29)


### Bug Fixes

* merge contant at wrong path ([a849961](https://gitlab.com/alline/scraper-constant/commit/a8499610de7aeaa469982dac74c75c02e895af6a))

## [1.1.1](https://gitlab.com/alline/scraper-constant/compare/v1.1.0...v1.1.1) (2020-06-29)


### Bug Fixes

* update dependencies ([66c7d6c](https://gitlab.com/alline/scraper-constant/commit/66c7d6c8548139c68582c245ce2f817a65b3a99c))

# [1.1.0](https://gitlab.com/alline/scraper-constant/compare/v1.0.1...v1.1.0) (2020-05-19)


### Features

* update @alline/core ([97afce3](https://gitlab.com/alline/scraper-constant/commit/97afce3ca20c6c4326345b4d898c31f6b339c917))

## [1.0.1](https://gitlab.com/alline/scraper-constant/compare/v1.0.0...v1.0.1) (2020-05-19)


### Bug Fixes

* update @alline/core ([226233f](https://gitlab.com/alline/scraper-constant/commit/226233feaa3ce5abadef733c28e80e3598453dca))

# 1.0.0 (2020-05-18)


### Features

* initial commit ([95225bf](https://gitlab.com/alline/scraper-constant/commit/95225bf6016b3725e40a271cdb688bbc7c2ac0ea))
